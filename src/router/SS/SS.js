
export const SSRender = {
    path: '/',
    component: {
        render(c) {
            return c('router-view')
        }
    },
    children: [        
    ]
}