import Vue from 'vue'
import Router from 'vue-router'

import {
  ADMRender
} from './ADM/ADM'

import {
  SSRender
} from './SS/SS'

import {
  DDRender
} from './DD/DD'

import {
  MKRender
} from './MK/MK'

import {
  OPRender
} from './OP/OP'

// CONTAINERS
import LayoutFull from '@/containers/LayoutFull/LayoutFull'

// COMPONENTS
import Dashboard from '@/views/Dashboard/Dashboard'
import MenuPages from '@/views/MenuPages/MenuPages'
import SignIn from '@/views/Authentication/SignIn/SignIn'
import PageNotFound from '@/views/PageNotFound/PageNotFound'
import Notification from '@/views/UserContent/Notification'
import DynamicMenu from '@/views/Setup/Dynamic_Menu'
import Tensi from '@/views/Vaccine/Tensi'
import TensiForm from '@/views/Vaccine/TensiForm'
import dashboard from '@/views/Vaccine/VerifikasiVaksin'
import user from '@/views/Vaccine/VerifikasiVaksinForm'
import Observasi from '@/views/Vaccine/Observasi'
import ObservasiForm from '@/views/Vaccine/ObservasiForm'
import DataCenter from '@/views/Vaccine/DataCenter'
import DataCenterForm from '@/views/Vaccine/DataCenterForm'
import EditDataCenter from '@/views/Vaccine/EditDataCenter'
import Site from '@/views/Vaccine/Site'
import Screening from '@/views/Vaccine/Screening'
import ScreeningForm from '@/views/Vaccine/ScreeningForm'
import CheckPasswordForm from '@/views/Vaccine/CheckPasswordForm'
import UploadData from '@/views/Vaccine/UploadData.vue'
import UploadDataForm from '@/views/Vaccine/UploadDataForm.vue'
import ScanQRCode from '@/views/Vaccine/ScanQRCode.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({
    y: 0
  }),
  routes: [{
    path: '/',
    redirect: '/',
    name: 'LayoutFull',
    component: LayoutFull,
    children: [{
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        requiredAuth: true,
        breadcrumbs: [{
          name: 'Dashboard'
        }]
      }
    },
    {
      path: '/menu',
      name: 'Menu',
      component: MenuPages,
      meta: {
        requiredAuth: true,
        breadcrumbs: [{
          name: 'Menu'
        }]
      }
    },
    {
      path: '/Notification',
      name: 'Notification',
      component: Notification,
      meta: {
        requiredAuth: true,
        breadcrumbs: [{
          name: 'Notification'
        }]
      }
    },
      ADMRender,
      SSRender,
      MKRender,
      OPRender,
      DDRender,
    {
      path: '/DynamicMenu',
      name: 'DynamicMenu',
      component: DynamicMenu,
      meta: {
        requiredAuth: true,
        breadcrumbs: [{
          name: 'Dynamic Menu'
        }]
      }
    },
    ]
  },
  {
    path: '/sign-in',
    name: 'sign-in',
    component: SignIn,
    meta: {
      requiredAuth: false
    }
  },
  {
    path: '*',
    component: PageNotFound
  },
  {
    path: '/Tensi',
    name: 'Tensi',
    component: Tensi,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/TensiForm',
    name: 'TensiForm',
    component: TensiForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: dashboard,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/user',
    name: 'user',
    component: user,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/Observasi',
    name: 'Observasi',
    component: Observasi,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/UploadData',
    name: 'UploadData',
    component: UploadData,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/UploadDataForm',
    name: 'UploadDataForm',
    component: UploadDataForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/ObservasiForm',
    name: 'ObservasiForm',
    component: ObservasiForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/DataCenter',
    name: 'DataCenter',
    component: DataCenter,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/DataCenterForm',
    name: 'DataCenterForm',
    component: DataCenterForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/Site',
    name: 'Site',
    component: Site,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/Screening',
    name: 'Screening',
    component: Screening,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/ScreeningForm',
    name: 'ScreeningForm',
    component: ScreeningForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/CheckPasswordForm',
    name: 'CheckPasswordForm',
    component: CheckPasswordForm,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/EditDataCenter',
    name: 'EditDataCenter',
    component: EditDataCenter,
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/ScanQRCode',
    name: 'ScanQRCode',
    component: ScanQRCode,
    meta: {
      requiredAuth: true
    }
  },
  ]
})